package db

import (
	"fmt"
	mydb "gintest/db/mysql"

	table "gintest/table"
	"log"
)

func UserSignup(username string, passwd string) bool {
	fmt.Println(username)
	fmt.Println(passwd)
	stmt, err := mydb.DBConn().Prepare(
		"insert ignore into tbl_user (`user_name`,`user_pwd`) values (?,?)")
	if err != nil {
		log.Println("Failed to insert, err:" + err.Error())

		return false
	}
	defer stmt.Close()
	ret, err := stmt.Exec(username, passwd)
	if err != nil {
		log.Println("Failed to insert, err:" + err.Error())

		return false
	}
	if rowsAffected, err := ret.RowsAffected(); nil == err && rowsAffected > 0 {

		return true
	}

	return false
}

func UserSignin(username string, encpwd string) bool {
	stmt, err := mydb.DBConn().Prepare("SELECT * from tbl_user where user_name=? limit 1")
	if err != nil {
		log.Println(err.Error())

		return false
	}
	defer stmt.Close()

	rows, err := stmt.Query(username)
	if err != nil {
		log.Println(err.Error())

		return false
	} else if rows == nil {
		log.Println("username not found: " + username)

		return false
	}

	pRows := mydb.ParseRows(rows)
	if len(pRows) > 0 && string(pRows[0]["user_pwd"].([]byte)) == encpwd {

		return true
	}

	return false
}

//
func UpdateToken(username string, token string) bool {
	stmt, err := mydb.DBConn().Prepare(
		"replace into tbl_user_token (`user_name`,`user_token`) values (?,?)")

	if err != nil {
		log.Println(err.Error())
		return false
	}
	defer stmt.Close()

	_, err = stmt.Exec(username, token)
	if err != nil {
		log.Println(err.Error())

		return false
	}

	return true
}

// GetUserInfo : 查询用户信息
func GetUserInfo(username string) (bool, table.TableUser) {
	user := table.TableUser{}

	stmt, err := mydb.DBConn().Prepare(
		"select user_name,signup_at from tbl_user where user_name=? limit 1")
	if err != nil {
		log.Println(err.Error())
		// error不为nil, 返回时user应当置为nil
		//return user, err

		return false, user
	}
	defer stmt.Close()

	// 执行查询的操作
	err = stmt.QueryRow(username).Scan(&user.Username, &user.SignupAt)
	if err != nil {

		return false, user
	}

	return true, user
}

// UserExist : 查询用户是否存在
func UserExist(username string) bool {
	stmt, err := mydb.DBConn().Prepare(
		"select 1 from tbl_user where user_name=? limit 1")
	if err != nil {
		log.Println(err.Error())

		return false
	}
	defer stmt.Close()

	_, err = stmt.Query(username)
	if err != nil {

		return false
	}

	return true
}
