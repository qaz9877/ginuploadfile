package db

import (
	"database/sql"
	mydb "gintest/db/mysql"
	"gintest/table"
	"log"
)

func OnFileUploadFinished(filehash string, filename string, filesize int64, fileaddr string) bool {

	stmt, err := mydb.DBConn().Prepare(
		"insert ignore into tbl_file (`file_sha1`,`file_name`,`file_size`," +
			"`file_addr`,`status`) values (?,?,?,?,1)")

	if err != nil {
		log.Println("Failed to prepare statement, err:" + err.Error())

		return false
	}
	defer stmt.Close()
	ret, err := stmt.Exec(filehash, filename, filesize, fileaddr)
	if err != nil {
		log.Println(err.Error())

		return false
	}
	if rf, err := ret.RowsAffected(); nil == err {
		if rf <= 0 {
			log.Printf("File with hash:%s has been uploaded before", filehash)

		}

		return true
	}

	return false
}

func GetFileMeta(filehash string) (bool, table.TableFile) {
	tfile := table.TableFile{}
	stmt, err := mydb.DBConn().Prepare(
		"select file_sha1,file_addr,file_name,file_size from tbl_file " +
			"where file_sha1=? and status=1 limit 1")
	if err != nil {
		log.Println(err.Error())

		return false, tfile
	}
	defer stmt.Close()

	err = stmt.QueryRow(filehash).Scan(
		&tfile.FileHash, &tfile.FileAddr, &tfile.FileName, &tfile.FileSize)
	if err != nil {
		if err == sql.ErrNoRows {
			// 查不到对应记录， 返回参数及错误均为nil

			return true, tfile
		} else {
			log.Println(err.Error())
			return false, tfile
		}
	}
	return true, tfile
}
