package db

import (
	mydb "gintest/db/mysql"
	table "gintest/table"
	"log"
	"time"
)

func OnUserFileUploadFinished(username, filehash, filename string, filesize int64) bool {
	stmt, err := mydb.DBConn().Prepare(
		"insert ignore into tbl_user_file (`user_name`,`file_sha1`,`file_name`," +
			"`file_size`,`upload_at`) values (?,?,?,?,?)")
	if err != nil {
		log.Println(err.Error())

		return false
	}
	defer stmt.Close()

	_, err = stmt.Exec(username, filehash, filename, filesize, time.Now())
	if err != nil {
		log.Println(err.Error())

		return false
	}

	return true
}

// QueryUserFileMetas : 批量获取用户文件信息
func QueryUserFileMetas(username string, limit int64) (bool, []table.TableUserFile) {
	var userFiles []table.TableUserFile
	stmt, err := mydb.DBConn().Prepare(
		"select file_sha1,file_name,file_size,upload_at," +
			"last_update from tbl_user_file where user_name=? limit ?")
	if err != nil {
		log.Println(err.Error())

		return false, userFiles
	}
	defer stmt.Close()

	rows, err := stmt.Query(username, limit)
	if err != nil {
		log.Println(err.Error())
		return false, userFiles
	}

	for rows.Next() {
		ufile := table.TableUserFile{}
		err = rows.Scan(&ufile.FileHash, &ufile.FileName, &ufile.FileSize,
			&ufile.UploadAt, &ufile.LastUpdated)
		if err != nil {
			log.Println(err.Error())
			break
		}
		userFiles = append(userFiles, ufile)
	}

	return true, userFiles
}

// QueryUserFileMeta : 获取用户单个文件信息
func QueryUserFileMeta(username string, filehash string) (bool, table.TableUserFile) {
	ufile := table.TableUserFile{}
	stmt, err := mydb.DBConn().Prepare(
		"select file_sha1,file_name,file_size,upload_at," +
			"last_update from tbl_user_file where user_name=? and file_sha1=?  limit 1")
	if err != nil {

		return false, ufile
	}
	defer stmt.Close()

	rows, err := stmt.Query(username, filehash)
	if err != nil {

		return false, ufile
	}

	if rows.Next() {
		err = rows.Scan(&ufile.FileHash, &ufile.FileName, &ufile.FileSize,
			&ufile.UploadAt, &ufile.LastUpdated)
		if err != nil {
			log.Println(err.Error())

			return false, ufile
		}
	}

	return true, ufile
}

// RenameFileName : 文件重命名
func RenameFileName(username, filehash, filename string) bool {
	stmt, err := mydb.DBConn().Prepare(
		"update tbl_user_file set file_name=? where user_name=? and file_sha1=? limit 1")
	if err != nil {
		log.Println(err.Error())

		return false
	}
	defer stmt.Close()

	_, err = stmt.Exec(filename, username, filehash)
	//	fmt.Println("//   ", filename, username, filehash)
	if err != nil {
		log.Println(err.Error())

		return false
	}

	return true
}
