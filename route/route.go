package route

import (
	"gintest/bindata"
	mysql "gintest/db/mysql"
	"gintest/handler"

	assetfs "github.com/elazarl/go-bindata-assetfs"
	"github.com/gin-gonic/gin"
)

func Router() *gin.Engine {
	mysql.InitDBConn()
	router := gin.Default()
	//加载静态资源，例如网页的css、js
	fs := assetfs.AssetFS{
		Asset:     bindata.Asset,
		AssetDir:  bindata.AssetDir,
		AssetInfo: nil,
		Prefix:    "static", //一定要加前缀
	}
	router.StaticFS("/static", &fs)
	//router.Static("/static/", "./static")
	router.GET("/user/signup", handler.SignupHandler)

	router.POST("/user/signup", handler.DoSingupHandler)

	router.GET("user/signin", handler.SigninHandler)
	router.POST("user/signin", handler.DoSigninHandler)
	router.Use(handler.Authorize())
	// 用户查询
	router.POST("/user/info", handler.UserInfoHandler)

	// 用户文件查询
	router.POST("/file/query", handler.FileQueryHandler)

	// 用户文件修改(重命名)
	router.POST("/file/update", handler.FileMetaUpdateHandler)

	// 用户文件下载 文件
	router.POST("/file/downloadurl", handler.FileMetadownloadHandler)

	//文件上传
	router.POST("/file/upload", handler.DoUploadHandler)

	// 秒传接口
	router.POST("/file/fastupload", handler.TryFastUploadHandler)

	// 分块上传接口
	router.POST("/file/mpupload/init", handler.InitialMultipartUploadHandler)
	router.POST("/file/mpupload/uppart", handler.UploadPartHandler)
	router.POST("/file/mpupload/complete", handler.CompleteUploadHandler)
	return router
}
