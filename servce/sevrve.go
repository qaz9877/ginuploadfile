package main

import (
	"context"
	"errors"
	"fmt"
	"gintest/message"
	"log"

	"github.com/micro/go-micro/v2"
)

type StudentServiceImpl struct {
}

//服务实现
func (ss *StudentServiceImpl) GetStudent(ctx context.Context, request *message.StudentRequest, restp *message.Student) error {
	studentMap := map[string]message.Student{
		"davie":  message.Student{Name: "davie", Classes: "软件工程", Grade: 80},
		"steven": message.Student{Name: "steven", Classes: "计算机科学与技术", Grade: 90},
		"tony":   message.Student{Name: "tony", Classes: "计算机网络工程", Grade: 85},
		"jack":   message.Student{Name: "jack", Classes: "工商管理", Grade: 96},
	}
	if request.Name == "" {
		return errors.New(" 请求参数错误,请重新请求。")
	}
	student := studentMap[request.Name]
	if student.Name != "" {
		fmt.Println(student.Name, student.Classes, student.Grade)
		*restp = student
		return nil
	}

	return errors.New("未查询相关学生信息")
}

func main() {

	service := micro.NewService(

		micro.Name("go.micro.srv.student"),
	)
	service.Init()

	//注册
	message.RegisterStudentServiceHandler(service.Server(), new(StudentServiceImpl))
	if err := service.Run(); err != nil {
		log.Fatal(err.Error())
	}

}
