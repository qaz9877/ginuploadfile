/*
SQLyog Ultimate v12.14 (64 bit)
MySQL - 5.7.36 : Database - file
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`file` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `file`;

/*Table structure for table `tbl_file` */

DROP TABLE IF EXISTS `tbl_file`;

CREATE TABLE `tbl_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件hash',
  `file_name` varchar(256) NOT NULL DEFAULT '' COMMENT '文件名',
  `file_size` bigint(20) DEFAULT '0' COMMENT '文件大小',
  `file_addr` varchar(1024) NOT NULL DEFAULT '' COMMENT '文件存储位置',
  `create_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日期',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态(可用/禁用/已删除等状态)',
  `ext1` int(11) DEFAULT '0' COMMENT '备用字段1',
  `ext2` text COMMENT '备用字段2',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_file_hash` (`file_sha1`),
  KEY `idx_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_file` */

insert  into `tbl_file`(`id`,`file_sha1`,`file_name`,`file_size`,`file_addr`,`create_at`,`update_at`,`status`,`ext1`,`ext2`) values 
(1,'e54942078b49a9b34ac52938f2873d2c757e394d','th.jpg',34483,'oss/e54942078b49a9b34ac52938f2873d2c757e394d','2022-07-29 05:28:35','2022-07-29 05:28:35',1,0,NULL),
(3,'a17d3df6782a86966f8ca9b48a3437ee1b9cf0aa','账户.txt',1464,'oss/a17d3df6782a86966f8ca9b48a3437ee1b9cf0aa','2022-07-29 05:50:12','2022-07-29 05:50:12',1,0,NULL),
(4,'90332c534ae15becb8fdeda147654cfc00268692','密码.txt',60,'D:/data/fileserver/90332c534ae15becb8fdeda147654cfc00268692','2022-07-29 06:09:11','2022-07-29 06:09:11',1,0,NULL),
(5,'1502d99eda23313d7fd094634fed0c5ffb0918cf','一键打开.html',2372,'D:/data/fileserver/1502d99eda23313d7fd094634fed0c5ffb0918cf','2022-07-29 06:13:38','2022-07-29 06:13:38',1,0,NULL);

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '用户名',
  `user_pwd` varchar(256) NOT NULL DEFAULT '' COMMENT '用户encoded密码',
  `email` varchar(64) DEFAULT '' COMMENT '邮箱',
  `phone` varchar(128) DEFAULT '' COMMENT '手机号',
  `email_validated` tinyint(1) DEFAULT '0' COMMENT '邮箱是否已验证',
  `phone_validated` tinyint(1) DEFAULT '0' COMMENT '手机号是否已验证',
  `signup_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '注册日期',
  `last_active` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后活跃时间戳',
  `profile` text COMMENT '用户属性',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '账户状态(启用/禁用/锁定/标记删除等)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_username` (`user_name`),
  KEY `idx_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tbl_user` */

insert  into `tbl_user`(`id`,`user_name`,`user_pwd`,`email`,`phone`,`email_validated`,`phone_validated`,`signup_at`,`last_active`,`profile`,`status`) values 
(1,'admin','def6d80a5901257d9855c399c15fa13ec433cd34','','',0,0,'2022-07-29 05:27:53','2022-07-29 05:27:53',NULL,0);

/*Table structure for table `tbl_user_file` */

DROP TABLE IF EXISTS `tbl_user_file`;

CREATE TABLE `tbl_user_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) NOT NULL,
  `file_sha1` varchar(64) NOT NULL DEFAULT '' COMMENT '文件hash',
  `file_size` bigint(20) DEFAULT '0' COMMENT '文件大小',
  `file_name` varchar(256) NOT NULL DEFAULT '' COMMENT '文件名',
  `upload_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '上传时间',
  `last_update` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '文件状态(0正常1已删除2禁用)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_file` (`user_name`,`file_sha1`),
  KEY `idx_status` (`status`),
  KEY `idx_user_id` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tbl_user_file` */

insert  into `tbl_user_file`(`id`,`user_name`,`file_sha1`,`file_size`,`file_name`,`upload_at`,`last_update`,`status`) values 
(1,'admin','e54942078b49a9b34ac52938f2873d2c757e394d',34483,'cc.jpg','2022-07-29 05:28:34','2022-07-29 06:02:48',0),
(3,'admin','a17d3df6782a86966f8ca9b48a3437ee1b9cf0aa',1464,'admin','2022-07-29 05:50:11','2022-07-29 05:54:59',0),
(4,'admin','90332c534ae15becb8fdeda147654cfc00268692',60,'密码.txt','2022-07-29 06:09:10','2022-07-29 06:09:11',0),
(5,'admin','1502d99eda23313d7fd094634fed0c5ffb0918cf',2372,'一键打开.html','2022-07-29 06:13:38','2022-07-29 06:13:38',0);

/*Table structure for table `tbl_user_token` */

DROP TABLE IF EXISTS `tbl_user_token`;

CREATE TABLE `tbl_user_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '用户名',
  `user_token` char(40) NOT NULL DEFAULT '' COMMENT '用户登录token',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_username` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tbl_user_token` */

insert  into `tbl_user_token`(`id`,`user_name`,`user_token`) values 
(9,'admin','e437408feaa08165a94e86d3d34ecce062e38397');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
