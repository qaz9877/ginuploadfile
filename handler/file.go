package handler

import (
	"bytes"
	"encoding/json"
	"fmt"
	cfg "gintest/config"
	dbcli "gintest/db"
	"gintest/mq"
	"gintest/store/ceph"
	"gintest/store/oss"
	table "gintest/table"
	"gintest/util"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"time"

	"github.com/gin-gonic/gin"
)

func DoUploadHandler(c *gin.Context) {
	errCode := 0
	defer func() {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		if errCode < 0 {

			c.JSON(http.StatusOK, gin.H{
				"code": errCode,
				"msg":  "上传失败",
			})
		} else {
			c.JSON(http.StatusOK, gin.H{
				"code": errCode,
				"msg":  "上传成功",
			})
		}
	}()

	// 1. 从form表单中获得文件内容句柄
	file, head, err := c.Request.FormFile("file")
	if err != nil {
		log.Printf("Failed to get form data, err:%s\n", err.Error())
		errCode = -1
		return
	}
	defer file.Close()
	buf := bytes.NewBuffer(nil)
	if _, err := io.Copy(buf, file); err != nil {
		log.Printf("Failed to get file data, err:%s\n", err.Error())
		errCode = -2
		return
	}

	fileMeta := table.FileMeta{
		FileName: head.Filename,
		FileSha1: util.Sha1(buf.Bytes()), //　计算文件sha1
		FileSize: int64(len(buf.Bytes())),
		UploadAt: time.Now().Format("2006-01-02 15:04:05"),
	}
	// 4. 将文件写入临时存储位置

	fileMeta.Location = cfg.TempLocalRootDir + fileMeta.FileSha1 // 临时存储地址

	err = os.MkdirAll(path.Dir(fileMeta.Location), 0744)
	if err != nil {
		fmt.Println(err)
		errCode = -3
		return
	}

	newFile, err := os.Create(fileMeta.Location)
	if err != nil {
		log.Printf("Failed to create file, err:%s\n", err.Error())
		errCode = -3
		return
	}

	defer newFile.Close()

	nByte, err := newFile.Write(buf.Bytes())
	if int64(nByte) != fileMeta.FileSize || err != nil {
		log.Printf("Failed to save data into file, writtenSize:%d, err:%s\n", nByte, err.Error())
		errCode = -4
		return
	}
	newFile.Seek(0, 0)
	if cfg.CurrentStoreType == cfg.StoreCeph {
		//文件写入cephC存储
		data, _ := ioutil.ReadAll(newFile)
		cephPath := cfg.CephRootDir + fileMeta.FileSha1
		_ = ceph.PutObject("userfile", cephPath, data)
		fileMeta.Location = cephPath

	} else if cfg.CurrentStoreType == cfg.StoreOSS {

		// 文件写入OSS存储
		ossPath := cfg.OSSRootDir + fileMeta.FileSha1
		if cfg.AsyncTransferEnable {

			// TODO: 设置oss中的文件名，方便指定文件名下载
			err = oss.Bucket().PutObject(ossPath, newFile)
			if err != nil {
				log.Println(err.Error())
				errCode = -5
				return
			}
			fileMeta.Location = ossPath
		} else {
			// 写入异步转移任务队列

			data := mq.TransferData{
				FileHash:      fileMeta.FileSha1,
				CurLocation:   fileMeta.Location,
				DestLocation:  ossPath,
				DestStoreType: cfg.StoreOSS,
			}
			pubData, _ := json.Marshal(data)
			pubSuc := mq.Publish(
				cfg.TransExchangeName,
				cfg.TransOSSRoutingKey,
				pubData,
			)
			if !pubSuc {
				// TODO: 当前发送转移信息失败，稍后重试
			}

		}
	}
	//6.  更新文件表记录strconv.FormatInt(int64,10)
	flag := dbcli.OnFileUploadFinished(fileMeta.FileSha1, fileMeta.FileName, fileMeta.FileSize, fileMeta.Location)
	if !flag {
		errCode = -6
		return
	}

	// 7. 更新用户文件表
	username := c.Request.FormValue("username")
	flag1 := dbcli.OnUserFileUploadFinished(username, fileMeta.FileSha1, fileMeta.FileName, fileMeta.FileSize)
	if flag1 {
		errCode = 0
	} else {
		errCode = -6
	}

}

func TryFastUploadHandler(c *gin.Context) {
	// 1. 解析请求参数
	username := c.Request.FormValue("username")
	filehash := c.Request.FormValue("filehash")
	filename := c.Request.FormValue("filename")
	// 2. 从文件表中查询相同hash的文件记录
	flag, fileMeta := dbcli.GetFileMeta(filehash)
	if !flag {
		//log.Println(err.Error())
		c.Status(http.StatusInternalServerError)
		return
	}
	if fileMeta.FileAddr.String == "" {
		resp := util.RespMsg{
			Code: -1,
			Msg:  "秒传失败，请访问普通上传接口",
		}
		c.Data(http.StatusOK, "application/json", resp.JSONBytes())
		return
	}
	// 4. 上传过则将文件信息写入用户文件表， 返回成功
	fmeta := TableFileToFileMeta(fileMeta)
	fmeta.FileName = filename
	falg1 := dbcli.OnUserFileUploadFinished(username, fmeta.FileSha1, fmeta.FileName, fmeta.FileSize)
	if falg1 {
		resp := util.RespMsg{
			Code: 0,
			Msg:  "秒传成功",
		}
		c.Data(http.StatusOK, "application/json", resp.JSONBytes())
		return
	}
	resp := util.RespMsg{
		Code: -2,
		Msg:  "秒传失败，请稍后重试",
	}
	c.Data(http.StatusOK, "application/json", resp.JSONBytes())
	return

}

func TableFileToFileMeta(tfile table.TableFile) table.FileMeta {
	return table.FileMeta{
		FileSha1: tfile.FileHash,
		FileName: tfile.FileName.String,
		FileSize: tfile.FileSize.Int64,
		Location: tfile.FileAddr.String,
	}
}
