package handler

import (
	"fmt"
	cfg "gintest/config"
	dbcli "gintest/db"
	"gintest/util"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func GenToken(username string) string {
	// 40位字符:md5(username+timestamp+token_salt)+timestamp[:8]
	ts := fmt.Sprintf("%x", time.Now().Unix())
	tokenPrefix := util.MD5([]byte(username + ts + "_tokensalt"))
	return tokenPrefix + ts[:8]
}

// SignupHandler : 响应注册页面
func SignupHandler(c *gin.Context) {
	c.Redirect(http.StatusFound, "/static/view/signup.html")
}

func DoSingupHandler(c *gin.Context) {
	username := c.Request.FormValue("username")
	passwd := c.Request.FormValue("password")
	if len(username) < 3 || len(passwd) < 5 {
		c.JSON(http.StatusOK, gin.H{
			"code": "400",
			"msg":  "注册参数无效",
		})
		return
	}

	// 对密码进行加盐及取Sha1值加密
	encPasswd := util.Sha1([]byte(passwd + cfg.PasswordSalt))
	// 将用户信息注册到用户表中
	flag := dbcli.UserSignup(username, encPasswd)
	if flag {
		c.JSON(http.StatusOK, gin.H{
			"code": "10000",
			"msg":  "注册成功",
		})

		return
	} else {
		c.JSON(http.StatusOK, gin.H{
			"code": "401",
			"msg":  "注册失败",
		})

	}

}

func SigninHandler(c *gin.Context) {
	c.Redirect(http.StatusFound, "/static/view/signin.html")
}

func DoSigninHandler(c *gin.Context) {
	username := c.Request.FormValue("username")
	password := c.Request.FormValue("password")
	encPasswd := util.Sha1([]byte(password + cfg.PasswordSalt))

	// 1. 校验用户名及密码
	falg := dbcli.UserSignin(username, encPasswd)
	if !falg {
		c.JSON(http.StatusOK, gin.H{
			"code": "401",
			"msg":  "登录失败",
		})

		return
	}
	// 2. 生成访问凭证(token)
	token := GenToken(username)
	falg1 := dbcli.UpdateToken(username, token)
	if !falg1 {
		c.Status(http.StatusInternalServerError)
		return
	}
	// 登录成功，返回用户信息
	cliResp := util.RespMsg{
		Code: 200,
		Msg:  "登录成功",
		Data: struct {
			Location      string
			Username      string
			Token         string
			UploadEntry   string
			DownloadEntry string
		}{
			Location: "/static/view/home.html",
			Username: username,
			Token:    token,
			// UploadEntry:   upEntryResp.Entry,
			// DownloadEntry: dlEntryResp.Entry,
			UploadEntry:   cfg.UploadLBHost,
			DownloadEntry: cfg.DownloadLBHost,
		},
	}
	c.Data(http.StatusOK, "application/json", cliResp.JSONBytes())

}

func UserInfoHandler(c *gin.Context) {
	username := c.Request.FormValue("username")

	//fmt.Println(username)
	flag, tableuser := dbcli.GetUserInfo(username)

	if !flag {
		c.JSON(http.StatusOK, gin.H{
			"code": "10005",
			"msg":  "查询失败",
		})
		return
	}

	// 3. 组装并且响应用户数据
	cliResp := util.RespMsg{
		Code: 0,
		Msg:  "OK",
		Data: gin.H{
			"Username": username,
			"SignupAt": tableuser.SignupAt,
			// TODO: 完善其他字段信息
			"LastActive": tableuser.LastActiveAt,
		},
	}

	//fmt.Println(tableuser)
	c.Data(http.StatusOK, "application/json", cliResp.JSONBytes())

}
