package handler

import (
	"encoding/json"
	"fmt"
	cfg "gintest/config"
	dbcli "gintest/db"
	"gintest/store/oss"
	"io"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

// FileQueryHandler : 查询批量的文件元信息
func FileQueryHandler(c *gin.Context) {
	limitCnt, _ := strconv.Atoi(c.Request.FormValue("limit"))
	username := c.Request.FormValue("username")

	flag, tablefilemetas := dbcli.QueryUserFileMetas(username, int64(limitCnt))
	if !flag || len(tablefilemetas) == 0 {
		c.Status(http.StatusInternalServerError)
		return
	}

	data, err := json.Marshal(tablefilemetas)
	if err != nil {
		c.Status(http.StatusInternalServerError)
		return
	}
	c.Data(http.StatusOK, "application/json", data)
}

// FileMetaUpdateHandler ： 更新元信息接口(重命名)
func FileMetaUpdateHandler(c *gin.Context) {
	opType := c.Request.FormValue("op")
	fileSha1 := c.Request.FormValue("filehash")
	username := c.Request.FormValue("username")
	newFileName := c.Request.FormValue("filename")

	if opType != "0" || len(newFileName) < 1 {
		c.Status(http.StatusForbidden)
		return
	}

	falg := dbcli.RenameFileName(username, fileSha1, newFileName)

	if !falg {
		c.Status(http.StatusInternalServerError)
		return
	}

	flag, tablefilemetas := dbcli.QueryUserFileMetas(username, int64(10))
	//fmt.Println(tablefilemetas)
	if !flag || len(tablefilemetas) == 0 {
		c.Status(http.StatusInternalServerError)
		return
	}

	data, err := json.Marshal(tablefilemetas)
	if err != nil {
		c.Status(http.StatusInternalServerError)
		return
	}
	c.Data(http.StatusOK, "application/json", data)
}

func FileMetadownloadHandler(c *gin.Context) {
	fileSha1 := c.Request.FormValue("filehash")
	username := c.Request.FormValue("username")
	flag, filemeta := dbcli.GetFileMeta(fileSha1)
	if !flag || filemeta.FileSize.Int64 == 0 {
		fmt.Println("11")
		c.Status(http.StatusInternalServerError)
		return
	}
	flag1, tablefilemeta := dbcli.QueryUserFileMeta(username, fileSha1)
	//fmt.Println(tablefilemetas)
	if !flag1 || tablefilemeta.FileSize == 0 {
		fmt.Println("22")
		c.Status(http.StatusInternalServerError)
		return
	}
	fpath := cfg.LocalPartDir + tablefilemeta.FileName
	os.MkdirAll(path.Dir(fpath), 0744)

	fmt.Println(filemeta.FileAddr.String)
	if strings.HasPrefix(filemeta.FileAddr.String, "oss/") {
		err := oss.Bucket().GetObjectToFile(filemeta.FileAddr.String, cfg.LocalPartDir+tablefilemeta.FileName)
		if err != nil {
			fmt.Println("33")
			fmt.Println(tablefilemeta.FileName)
			c.Status(http.StatusInternalServerError)
			return
		}
	} else {
		fmt.Println("44")
		fmt.Println(tablefilemeta.FileName)
		copy(cfg.TempLocalRootDir+fileSha1, cfg.LocalPartDir+tablefilemeta.FileName)
	}

	c.JSON(
		http.StatusOK,
		gin.H{
			"code": 0,
			"msg":  "OK",
			"data": tablefilemeta.FileName,
		})

}

func copy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}
