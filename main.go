package main

import "gintest/route"

func main() {
	r := route.Router()
	r.Run(":8080")
}
