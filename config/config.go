package config

import "fmt"

// ErrorCode : 错误码
type ErrorCode int32

const (
	_ int32 = iota + 9999
	// StatusOK : 正常
	StatusOK
	// StatusParamInvalid : 请求参数无效
	StatusParamInvalid
	// StatusServerError : 服务出错
	StatusServerError
	// StatusRegisterFailed : 注册失败
	StatusRegisterFailed
	// StatusLoginFailed : 登录失败
	StatusLoginFailed
	// StatusTokenInvalid : 10005 token无效
	StatusTokenInvalid
	// StatusUserNotExists: 10006 用户不存在
	StatusUserNotExists
)

type StoreType int

const (
	_ StoreType = iota
	// StoreLocal : 节点本地
	StoreLocal
	// StoreCeph : Ceph集群
	StoreCeph
	// StoreOSS : 阿里OSS
	StoreOSS
	// StoreMix : 混合(Ceph及OSS)
	StoreMix
	// StoreAll : 所有类型的存储都存一份数据
	StoreAll
)

var (
	// MySQLSource : 要连接的数据库源；
	// 其中test:test 是用户名密码；
	// 127.0.0.1:3306 是ip及端口；
	// fileserver 是数据库名;
	// charset=utf8 指定了数据以utf8字符编码进行传输
	// UploadServiceHost : 上传服务监听的地址
	UploadServiceHost = "0.0.0.0:8080"
	// UploadLBHost: 上传服务LB地址
	UploadLBHost = "http://localhost:8080"
	// DownloadLBHost: 下载服务LB地址
	DownloadLBHost = "http://localhost:8080"
	// TracerAgentHost: tracing agent地址
	TracerAgentHost = "127.0.0.1:6831"

	// TempLocalRootDir : 本地临时存储地址的路径
	TempLocalRootDir = "/data/fileserver/"
	// TempPartRootDir : 分块文件在本地临时存储地址的路径
	TempPartRootDir = "/data/fileserver_part/"
	//C:/Users/ASUS/Desktop/
	LocalPartDir = "/data/docload/"
	// CephRootDir : Ceph的存储路径prefix
	CephRootDir = "/ceph"
	// OSSRootDir : OSS的存储路径prefix
	OSSRootDir = "oss/"
	// CurrentStoreType : 设置当前文件的存储类型
	CurrentStoreType = StoreOSS
	// CephAccessKey : 访问Key
	CephAccessKey = "PFEA7NXWXSOWVTFA16C9"
	// CephSecretKey : 访问密钥
	CephSecretKey = "cf3dwPMeadGbtEgwFUEA6emRVrVfDHpv0pLXFYby"
	// CephGWEndpoint : gateway地址
	CephGWEndpoint = "http://<你的rgw_host>:<<你的rgw_port>>"

	// OSSBucket : oss bucket名
	OSSBucket = "qgua"
	// OSSEndpoint : oss endpoint
	OSSEndpoint = "oss-cn-hangzhou.aliyuncs.com"
	// OSSAccesskeyID : oss访问key
	OSSAccesskeyID = "PFEA7NXWXSOWVTFA16C9"
	// OSSAccessKeySecret : oss访问key secret
	OSSAccessKeySecret = "cf3dwPMeadGbtEgwFUEA6emRVrVfDHpv0pLXFYby"

	// AsyncTransferEnable : 是否开启文件异步转移(默认同步)
	AsyncTransferEnable = false
	// TransExchangeName : 用于文件transfer的交换机

	// RabbitURL : rabbitmq服务的入口url
	RabbitURL = "amqp://guest:guest@192.168.0.102:5672/"

	TransExchangeName = "uploadserver.trans"
	// TransOSSQueueName : oss转移队列名
	TransOSSQueueName = "uploadserver.trans.oss"
	// TransOSSErrQueueName : oss转移失败后写入另一个队列的队列名
	TransOSSErrQueueName = "uploadserver.trans.oss.err"
	// TransOSSRoutingKey : routingkey
	TransOSSRoutingKey = "oss"

	PasswordSalt = "#890"
	MySQLSource  = "root:123456@tcp(192.168.0.102:3306)/file?charset=utf8"
)

func UpdateDBHost(host string) {
	MySQLSource = fmt.Sprintf("root:123456@tcp(%s)/file?charset=utf8", host)
}
