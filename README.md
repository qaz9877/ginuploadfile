 
修改config 信息  osskey  redis mysql  mq路径    下载路径
![登入](test/4644.png)
![登入](test/41.png)
![登入](test/614.png)


 **_

### 访问路劲
_** 
http://localhost:8080/user/signup
**登入** 
![登入](test/登入.png)

 **home页面 ** 
![home页面](test/home.png)

 **上传页面** 
![上传页面](test/上传.png)

 **上传效果** 
![上传效果](image.png)

 **断点效果** 
![断点效果](test/断点.png)

 **redis数据** 
![redis数据](test/redis.png)

 **mq效果** 
![mq效果](test/mq.png)

 **oss存储** 
![oss存储](test/oss.png)

